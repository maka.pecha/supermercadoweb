function validarAltaUsuario() {
    var txtNombre = document.getElementById('txtNombre');
    var txtContraseña = document.getElementById('txtContraseña');


    if(txtNombre.value ===""){
        alert("DEBE INGRESAR EL NOMBRE");
        txtNombre.focus();
        return false;
    }

     
    if(txtContraseña.value ===""){
        alert("DEBE INGRESAR LA CONTRASEÑA");
        txtContraseña.focus();
        return false;
    }
    
    return true;
}


function validarAltaOferta() {
    var txtTitulo = document.getElementById('txtTitulo');
    var txtPrecio_real = document.getElementById('txtPrecio_real');
    var txtPrecio_descuento = document.getElementById('txtPrecio_descuento');
    var txtFecha = document.getElementById('txtFecha');

    if(txtPrecio_real.value ===""){
        alert("DEBE INGRESAR EL PRECIO");
        txtPrecio_real.focus();
        return false;
    }
    
    if(txtPrecio_descuento.value ===""){
        alert("DEBE INGRESAR EL DESCUENTO");
        txtPrecio_descuento.focus();
        return false;
    }

    if(txtTitulo.value ===""){
        alert("DEBE INGRESAR EL TITULO");
        txtTitulo.focus();
        return false;
    }
    if(txtFecha.value ===""){
        alert("DEBE INGRESAR LA FECHA");
        txtFecha.focus();
        return false;
    }

    if (isNaN(parseFloat(txtPrecio_real.value)) || isNaN(parseFloat(txtPrecio_descuento.value))) {
        alert('El campo debe ser un número');
        txtPrecio_real.focus();
        return false;
    }
    return true;
}


function validarAltaRubro() {
    var txtNombre = document.getElementById('txtNombre');

    if(txtNombre.value ===""){
        alert("DEBE INGRESAR EL NOMBRE DEL RUBRO");
        txtNombre.focus();
        return false;
    }

    return true;
}

