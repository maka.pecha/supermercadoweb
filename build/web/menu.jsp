<%-- 
    Document   : menu
    Created on : 9 nov. 2020, 12:59:10
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <header>
        <nav id="nav" class="navbar nav-transparent">
            <div class="container">
                <div class="navbar-header">
                    <div class="navbar-brand">
                        <a href="https://www.frc.utn.edu.ar/"> 
                            <img class="logo" src="Assets/Images/front/logoutn.png" alt="logo">
                            <img class="logo-alt" src="Assets/Images/front/logoutn2.png" alt="logo">
                        </a>
                    </div>
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                 </div>
                <ul class="main-nav nav navbar-nav navbar-right tamañoMenu">
                    <li class="active"><a href="index.jsp">Home</a></li>
                    <li class="has-dropdown"><a>Ofertas</a>
                        <ul class="dropdown">
                            <li><a href="/SupermercadoWeb/ListadoOfertas">Listado de Ofertas</a></li>
                            <li><a href="/SupermercadoWeb/AgregarOferta">Alta de Ofertas</a></li>
                        </ul>
                    </li>
                    <li class="has-dropdown"><a>Productos</a>
                        <ul class="dropdown">
                            <li><a href="/SupermercadoWeb/ListadoProductos">Listado de Productos</a></li>
                            <li><a href="/SupermercadoWeb/AgregarProducto">Alta de Productos</a></li>
                        </ul>
                    </li>
                    <li><a href="/SupermercadoWeb/Reportes">Reportes</a></li>
                </ul>
            </div>
        </nav>
    </header>
</html>
