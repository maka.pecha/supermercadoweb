<%-- 
    Document   : agregarOferta
    Created on : 9 nov. 2020, 0:58:39
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
   <head>
            <%@include file="head.jsp" %>
            <title>Agregar Oferta</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>

<!-------------------------------------------HOME---------------------------------------------------->      
  <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal table-wrapper-scroll-y my-custom-scrollbar2">

                <div class="secundarioCliente ">
                    <a href="/SupermercadoWeb/ListadoOfertas" style="color: #fff" class="float-right"> Volver al Listado</a>
                    <h1 style="padding-left: 5%">OFERTA</h1>
        <form  method="POST" action="/SupermercadoWeb/AgregarOferta" onsubmit="return validar()">
            <input hidden="" type="number" name="txtId" value="${id}" class="form-control" id="txtId">
             <div class="form-group">
              <label for="txtProducto">PRODUCTO</label>
              <select name="txtProducto" id="txtProducto" class="btn btn-danger">
                  <c:forEach items="${listaP}" var="p">
                      <option value="${p.getId()}"<c:if test="${p.getId() == idProductoSelected}">selected</c:if>>${p.getNombre()}</option>
                </c:forEach>
              </select>
            </div>
            <div class="form-group">
              <label for="txtPrecioNormal">PRECIO</label>
              <input name="txtPrecioNormal" type="number" id="txtPrecioNormal" required value="${precioNormal}"  placeholder="Ingrese Precio" class="form-control"/>
            </div>
            <div class="form-group">
              <label for="txtPrecioOferta">OFERTA</label>
              <input type="number" name="txtPrecioOferta" value="${precioOferta}" class="form-control" id="txtPrecioOferta" placeholder="Ingrese Oferta">
            </div>
            <div class="form-group">
                <label for="txtStockDisponible">STOCK DISPONIBLE</label>
                <input type="number" name="txtStockDisponible" value="${stockDisponible}" class="form-control" id="txtStockDisponible" placeholder="Ingrese Stock Disponible" required>
              </div>
            <div class="form-group">
                <label for="txtFechaInicioOferta">FECHA INICIO DE OFERTA</label>
                <input type="date" name="txtFechaInicioOferta" value="${fechaInicioOferta}" class="form-control" id="txtFechaInicioOferta" placeholder="Ingrese Fecha Inicio de Oferta" required>
            </div>
            <div class="form-group">
                <label for="txtDiasVigencia">DÍAS DE VIGENCIA</label>
                <input type="number" name="txtDiasVigencia" value="${diasVigencia}" class="form-control" id="txtDiasVigencia" placeholder="Ingrese Días de Vigencia" required>
            </div>

            <button type="submit" onclick="validarAltaOferta()" class="btn btn-primary float-right">AGREGAR</button>
        </form>
                </div>
            </div>
    </div>
   
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
