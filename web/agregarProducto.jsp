<%-- 
    Document   : agregarProducto
    Created on : 2 dic. 2020, 15:56:57
    Author     : Maka
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
   <head>
            <%@include file="head.jsp" %>
            <title>Agregar Producto</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>

<!-------------------------------------------HOME---------------------------------------------------->      
  <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal table-wrapper-scroll-y my-custom-scrollbar2">

                <div class="secundarioCliente ">
                    <a href="/SupermercadoWeb/ListadoProductos" style="color: #fff" class="float-right"> Volver al Listado</a>
                    <h1 style="padding-left: 5%">PRODUCTO</h1>
        <form  method="POST" action="/SupermercadoWeb/AgregarProducto">
            <input hidden="" type="number" name="txtId" value="${id}" class="form-control" id="txtId">

            <div class="form-group">
              <label for="txtNombre">NOMBRE</label>
              <input name="txtNombre" type="text" id="txtNombre" required value="${nombre}"  placeholder="Ingrese Nombre" class="form-control"/>
            </div>
            <button type="submit" class="btn btn-primary float-right">AGREGAR</button>
        </form>
                </div>
            </div>
    </div>
   
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
