<%-- 
    Document   : listadoProductos
    Created on : 2 dic. 2020, 15:57:30
    Author     : Maka
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Productos</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <h1 style="padding-left: 5%">LISTADO DE PRODUCTOS</h1>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">NOMBRE</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaP}" var="p">      
                                <tr>
                                  <th scope="row">${p.getId()}</th>
                                  <td>${p.getNombre()}</td>
                                  <td><a href="/SupermercadoWeb/AgregarProducto?id=${p.getId()}"><button type="button" class="btn btn-outline-info">Editar</button></a></td>
                                  <td><a href="/SupermercadoWeb/EliminarProducto?id=${p.getId()}"><button type="button" class="btn btn-outline-danger">Eliminar</button></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>

