<%-- 
    Document   : reportes
    Created on : 2 dic. 2020, 20:05:41
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Supermercado</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
      
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">
                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <h1 style="padding-left: 5%">REPORTES</h1>
                    <h4 style="padding-left: 3%; padding-top: 20px; color: white">Cantidad de articulos ofertados por mas de 5 días: <span style="color: black">${reporte1}</span></h2>
                    <hr>
                    <h4 style="padding-left: 3%; padding-top: 20px; color: white">Monto perdido por tener en oferta el producto: <span style="color: black">${reporte2}</span></h2>
                    <form  method="POST" action="/SupermercadoWeb/Reportes?modo=filtro">
                        <div style="padding-left: 3%;" class="form-group">
                            <label for="txtProducto">PRODUCTO</label>
                            <select name="txtProducto" id="txtProducto" class="btn btn-danger">
                                <c:forEach items="${listaP}" var="p">
                                    <option value="${p.getId()}"<c:if test="${p.getId() == idProductoSelected}">selected</c:if>>${p.getNombre()}</option>
                              </c:forEach>
                            </select>
                            <button type="submit" class="btn btn-primary">CONSULTAR</button>
                        </div>
                    </form>
                    <br>
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>