<%-- 
    Document   : index
    Created on : 8 nov. 2020, 6:34:57
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Home</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>

<!-------------------------------------------HOME---------------------------------------------------->      
    <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal">
                  <div class="container">
                        <div class="indx">
                            <div >
                                <div class="home-content">
                                    <img class="img-thumbnail" style="width:250px; height:auto;" src="Assets/Images/front/comercio.png">
                                    <h1 class="white-text">Supermercado</h1>
                                    <p class="white-text">
                                        Práctica para el Final 2020
                                    </p>
                                    <p class="white-text">
                                        Laboratorio IV 
                                    </p>
                                    <p class="white-text">
                                        PECHA, Janet Macarena
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
   
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
