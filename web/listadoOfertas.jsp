<%-- 
    Document   : listadoOfertas
    Created on : 2 dic. 2020, 15:57:16
    Author     : Maka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <%@include file="head.jsp" %>
            <title>Ofertas</title>
    </head>
  
  <body style="">
    <%@include file="menu.jsp" %>
<!-------------------------------------------HOME---------------------------------------------------->      
        <div id="home">
            <div class="bg-img" style="background-image: url(&#39;Assets/Images/front/fondo.jpg&#39;);">
                <div class="overlay"></div>
            </div>  

            <div class="principal ">

                <div class="secundarioCliente table-wrapper-scroll-y my-custom-scrollbar2">
                    <h1 style="padding-left: 5%">LISTADO DE OFERTAS</h1>
                    <table class="table table-hover table-dark">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">PRODUCTO</th>
                            <th scope="col">PRECIO</th>
                            <th scope="col">OFERTA</th>
                            <th scope="col">STOCK</th>
                            <th scope="col">FECHA INICIO OFERTA</th>
                            <th scope="col">DÍAS DE VIGENCIA</th>
                          </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listaO}" var="o">      
                                <tr>
                                  <th scope="row">${o.getId()}</th>
                                  <td>${o.getProducto()}</td>
                                  <td>$ ${o.getPrecioNormal()}</td>
                                  <td>$ ${o.getPrecioOferta()}</td>
                                  <td>${o.getStockDisponible()}</td>
                                  <td>${o.getFechaInicioOferta()}</td>
                                  <td>${o.getDiasVigencia()}</td>
                                  <td><a href="/SupermercadoWeb/AgregarOferta?id=${o.getId()}"><button type="button" class="btn btn-outline-info">Editar</button></a></td>
                                  <td><a href="/SupermercadoWeb/EliminarOferta?id=${o.getId()}"><button type="button" class="btn btn-outline-danger">Eliminar</button></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
 <!-------------------------------------------FIN HOME------------------------------------------------>     
    <%@include file="footer.jsp" %>
</body>
</html>
