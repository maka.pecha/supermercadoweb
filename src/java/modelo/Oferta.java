/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Maka
 */
public class Oferta {
    private int id;
    private Producto producto;
    private double precioNormal;
    private double precioOferta;
    private int stockDisponible;
    private String fechaInicioOferta;
    private int diasVigencia;

    public Oferta(int id, Producto producto, double precioNormal, double precioOferta, int stockDisponible, String fechaInicioOferta, int diasVigencia) {
        this.id = id;
        this.producto = producto;
        this.precioNormal = precioNormal;
        this.precioOferta = precioOferta;
        this.stockDisponible = stockDisponible;
        this.fechaInicioOferta = fechaInicioOferta;
        this.diasVigencia = diasVigencia;
    }

    public Oferta() {
    }
    
    public int getId() {
        return id;
    }

    public Producto getProducto() {
        return producto;
    }

    public double getPrecioNormal() {
        return precioNormal;
    }

    public double getPrecioOferta() {
        return precioOferta;
    }

    public int getStockDisponible() {
        return stockDisponible;
    }

    public String getFechaInicioOferta() {
        return fechaInicioOferta;
    }

    public int getDiasVigencia() {
        return diasVigencia;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public void setPrecioNormal(double precioNormal) {
        this.precioNormal = precioNormal;
    }

    public void setPrecioOferta(double precioOferta) {
        this.precioOferta = precioOferta;
    }

    public void setStockDisponible(int stockDisponible) {
        this.stockDisponible = stockDisponible;
    }

    public void setFechaInicioOferta(String fechaInicioOferta) {
        this.fechaInicioOferta = fechaInicioOferta;
    }

    public void setDiasVigencia(int diasVigencia) {
        this.diasVigencia = diasVigencia;
    }

    @Override
    public String toString() {
        return "Oferta{" + "id=" + id + ", producto=" + producto.getNombre() + ", precioNormal=" + precioNormal + ", precioOferta=" + precioOferta + ", stockDisponible=" + stockDisponible + ", fechaInicioOferta=" + fechaInicioOferta + ", diasVigencia=" + diasVigencia + '}';
    }
    
}
