/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import gestor.GestorDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Producto;

/**
 *
 * @author Maka
 */
@WebServlet(name = "Reportes", urlPatterns = {"/Reportes"})
public class Reportes extends HttpServlet {
    GestorDB gestor = new GestorDB();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int reporte1 = gestor.cantidadProductosOfertadosMas5Dias();
        ArrayList<Producto> lista = new ArrayList<>();
        lista = gestor.obtenerProductos();
        request.setAttribute("reporte1", reporte1);
        request.setAttribute("listaP", lista);

        RequestDispatcher rd = getServletContext().getRequestDispatcher("/reportes.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idProducto = Integer.parseInt(request.getParameter("txtProducto"));
        double reporte2 = gestor.obtenerMontoPorOferta(idProducto);
        request.setAttribute("reporte2", "$ "+reporte2);
        
        int reporte1 = gestor.cantidadProductosOfertadosMas5Dias();
        ArrayList<Producto> lista = new ArrayList<>();
        lista = gestor.obtenerProductos();
        request.setAttribute("reporte1", reporte1);
        request.setAttribute("listaP", lista);

        String modo = request.getParameter("modo");
        if (modo.equals("filtro")) {
            request.setAttribute("idProductoSelected", idProducto);
        }

        System.out.println(idProducto+" idProducto");
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/reportes.jsp");
        rd.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
