/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import gestor.GestorDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Oferta;
import modelo.Producto;

/**
 *
 * @author Maka
 */
@WebServlet(name = "AgregarProducto", urlPatterns = {"/AgregarProducto"})
public class AgregarProducto extends HttpServlet {
    GestorDB gestor = new GestorDB();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         if (request.getParameter("id") == null) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarProducto.jsp");
            rd.forward(request, response);
        } else {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println(id+" id de producto a editar");
            Producto p = gestor.ObtenerProductoPorId(id);
            System.out.println(p + " producto para editar");
            
            request.setAttribute("id", p.getId());
            request.setAttribute("nombre", p.getNombre());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarProducto.jsp");
            rd.forward(request, response);
        }    
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id;
        if (request.getParameter("txtId").equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(request.getParameter("txtId"));
        }
        String nombre = request.getParameter("txtNombre");

        Producto producto = new Producto(id, nombre);
        
        System.out.println(producto +" producto");
        if(id == 0){
            gestor.agregarProducto(producto);
        }
        else {
            if (gestor.editarProducto(producto)) {
                System.out.println("Se EDITÓ correctamente"); 
            }
            else{System.out.println("NO Se EDITÓ correctamente"); }
        }
        response.sendRedirect("/SupermercadoWeb/ListadoProductos");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
