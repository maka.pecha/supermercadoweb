/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import gestor.GestorDB;
import modelo.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Maka
 */
@WebServlet(name = "AgregarOferta", urlPatterns = {"/AgregarOferta"})
public class AgregarOferta extends HttpServlet {
    GestorDB gestor = new GestorDB();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Producto> lista = new ArrayList<>();
        lista = gestor.obtenerProductos();
        request.setAttribute("listaP", lista);
         if (request.getParameter("id") == null) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarOferta.jsp");
            rd.forward(request, response);
        } else {
            int id = Integer.parseInt(request.getParameter("id"));
            System.out.println(id+" id de oferta a editar");
            Oferta o = gestor.ObtenerOfertaPorId(id);
            System.out.println(o + "oferta para editar");
            
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
            Date fecha = null;
            try {
                fecha = formatter.parse(o.getFechaInicioOferta());
            } catch (ParseException ex) {
                Logger.getLogger(AgregarOferta.class.getName()).log(Level.SEVERE, null, ex);
            }
            String fechaFormateada = formatter2.format(fecha);
            
            request.setAttribute("id", o.getId());
            request.setAttribute("idProductoSelected", o.getProducto().getId());
            request.setAttribute("precioNormal", o.getPrecioNormal());
            request.setAttribute("precioOferta", o.getPrecioOferta());
            request.setAttribute("stockDisponible", o.getStockDisponible());
            request.setAttribute("fechaInicioOferta", fechaFormateada);
            request.setAttribute("diasVigencia", o.getDiasVigencia());
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/agregarOferta.jsp");
            rd.forward(request, response);
        }    
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id;
        Producto producto = new Producto();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
        if (request.getParameter("txtId").equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(request.getParameter("txtId"));
        }
        int idProducto = Integer.parseInt(request.getParameter("txtProducto"));
        double precioNormal = Double.parseDouble(request.getParameter("txtPrecioNormal"));
        double precioOferta = Double.parseDouble(request.getParameter("txtPrecioOferta"));
        int stockDisponible = Integer.parseInt(request.getParameter("txtStockDisponible"));
        
        String fechaInicioOferta = request.getParameter("txtFechaInicioOferta");
        Date fecha = null;
        try {
            fecha = formatter.parse(fechaInicioOferta);
        } catch (ParseException ex) {
            Logger.getLogger(AgregarOferta.class.getName()).log(Level.SEVERE, null, ex);
        }
        String fechaFormateada = formatter2.format(fecha);
        System.out.println(fechaFormateada+" fecha formateada");
        
        
        int diasVigencia = Integer.parseInt(request.getParameter("txtDiasVigencia"));
        producto.setId(idProducto);
        
        Oferta o = new Oferta(id, producto, precioNormal, precioOferta, stockDisponible, fechaFormateada, diasVigencia);
        
        System.out.println(o +" ofertaaa");
        if(id == 0){
            gestor.agregarOferta(o);
        }
        else {
            if (gestor.editarOferta(o)) {
                System.out.println("Se EDITÓ correctamente"); 
            }
            else{System.out.println("NO Se EDITÓ correctamente"); }
        }
        response.sendRedirect("/SupermercadoWeb/ListadoOfertas");
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
