package gestor;

import dto.OfertaDTO;
import java.sql.*;
import java.util.ArrayList;
import modelo.*;
/**
 *
 * @author Maka
 */
public class GestorDB {
    private Connection con;
    private String CONN = "jdbc:sqlserver://localhost\\SQLEXPRESS:1434;databaseName=Supermercado";
    private String USER= "sa";
    private String PASS= "38161112";
    
    public void abrirConexion() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(CONN, USER, PASS);
        } catch (Exception exc){
            exc.printStackTrace();
        }
    }
    
    public void cerrarConexion(){
        try {
            if(con != null && !con.isClosed()){
                con.close();
            }
        } catch (Exception exc){
            exc.printStackTrace();
        }
    }
    
    public void agregarProducto(Producto p){
        
        try {
            
            abrirConexion();
            
            PreparedStatement ps = con.prepareStatement("INSERT INTO Producto VALUES (?)");
            ps.setString(1,p.getNombre());
            ps.executeUpdate();
            
            ps.close();
           
        } 
        catch (Exception exc) {
            exc.printStackTrace();
        }
       finally
        {
            cerrarConexion();
        }
    }
    
    public ArrayList<Producto> obtenerProductos(){
        ArrayList<Producto> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "SELECT * FROM Producto ORDER BY nombre";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                Producto v = new Producto(id, nombre);
                lista.add(v);
            }
        
        } catch(Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
    public Producto ObtenerProductoPorId(int idProducto){
        Producto producto = new Producto();
        try {
            abrirConexion();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, nombre\n" +
"				FROM Producto \n" +
"				WHERE id =" + idProducto);
          
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                
                producto = new Producto(id, nombre);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            cerrarConexion();
        }
        return producto;
    }
    
    public Boolean editarProducto(Producto editar){
        boolean flag = false;
        try {
            abrirConexion();
            PreparedStatement ps = con.prepareStatement("UPDATE Producto\n" +
"			SET nombre = ? \n" +
"			WHERE id = ?");
            ps.setString(1, editar.getNombre());
            ps.setInt(2, editar.getId());
            ps.executeUpdate();
            ps.close();
            flag= true;
        } catch (SQLException ex) {
            flag= false;
            ex.printStackTrace();
        }
        finally{
            cerrarConexion();
        }
        return flag;
    }
    
    public void eliminarProducto(int id){
        try {
            abrirConexion();
            PreparedStatement ps = con.prepareStatement("delete from Producto where id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            cerrarConexion();
        }
    }
        
    public void agregarOferta(Oferta o){
        try{
            abrirConexion();
            
            String query = "INSERT INTO Oferta VALUES (?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareCall(query);
            ps.setInt(1, o.getProducto().getId());
            ps.setDouble(2, o.getPrecioNormal());
            ps.setDouble(3, o.getPrecioOferta());
            ps.setInt(4, o.getStockDisponible());
            ps.setString(5, o.getFechaInicioOferta());
            ps.setInt(6, o.getDiasVigencia());
            ps.executeUpdate();
            ps.close();
            
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally
        {
            cerrarConexion();
        }
    }
    
    public Oferta ObtenerOfertaPorId(int idOferta){
        Oferta oferta = new Oferta();
        try {
            abrirConexion();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id, idProducto, precioNormal, precioOferta, stockDisponible, fechaInicioOferta, diasVigencia\n" +
"				FROM Oferta \n" +
"				WHERE id =" + idOferta);
          
            while (rs.next()) {
                int id = rs.getInt("id");
                int idProducto = rs.getInt("idProducto");
                double precioNormal = rs.getDouble("precioNormal");
                double precioOferta = rs.getDouble("precioOferta");
                int stockDisponible = rs.getInt("stockDisponible");
                String fechaInicioOferta = rs.getString("fechaInicioOferta");
                int diasVigencia = rs.getInt("diasVigencia");
                
                Producto producto = new Producto();
                producto.setId(idProducto);
                
                oferta = new Oferta(id, producto, precioNormal, precioOferta, stockDisponible, fechaInicioOferta, diasVigencia);
            }
            rs.close();
            st.close();
        } catch (SQLException exc) {
            exc.printStackTrace();
        }
        finally{
            cerrarConexion();
        }
        return oferta;
    }
    
    public Boolean editarOferta(Oferta editar){
        boolean flag = false;
        try {
            abrirConexion();
            PreparedStatement ps = con.prepareStatement("UPDATE Oferta\n" +
"			SET idProducto = ?, \n" +
"				precioNormal = ?, \n" +
"				precioOferta = ?, \n" +
"				stockDisponible = ?, \n" +
"				fechaInicioOferta = ?, \n" +
"				diasVigencia = ? \n" +
"			WHERE id = ?");
            ps.setInt(1, editar.getProducto().getId());
            ps.setDouble(2, editar.getPrecioNormal());
            ps.setDouble(3, editar.getPrecioOferta());
            ps.setInt(4, editar.getStockDisponible());
            ps.setString(5, editar.getFechaInicioOferta());
            ps.setInt(6, editar.getDiasVigencia());
            ps.setInt(7, editar.getId());
            ps.executeUpdate();
            ps.close();
            flag= true;
        } catch (SQLException ex) {
            flag= false;
            ex.printStackTrace();
        }
        finally{
            cerrarConexion();
        }
        return flag;
    }
        
    public void eliminarOferta(int id){
        try {
            abrirConexion();
            PreparedStatement ps = con.prepareStatement("delete from Oferta where id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally{
            cerrarConexion();
        }
    }
    
    public ArrayList<OfertaDTO> obtenerOfertasDTO(){
        ArrayList<OfertaDTO> lista = new ArrayList<>();
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            String query = "select o.id, p.nombre, o.precioNormal, o.precioOferta, o.stockDisponible, o.fechaInicioOferta, o.diasVigencia\n" +
                            "from Oferta o\n" +
                            "join Producto p on p.id = o.idProducto order by o.fechaInicioOferta desc";
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next()){
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                double precioNormal = rs.getDouble("precioNormal");
                double precioOferta = rs.getDouble("precioOferta");
                int stockDisponible = rs.getInt("stockDisponible");
                String fechaInicioOferta = rs.getString("fechaInicioOferta");
                int diasVigencia = rs.getInt("diasVigencia");
                
                OfertaDTO o = new OfertaDTO(id, nombre, precioNormal, precioOferta, stockDisponible, fechaInicioOferta, diasVigencia);
                lista.add(o);
            }
            rs.close();
            st.close();
            
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            cerrarConexion();
        }
        return lista;
    }
    
    public int cantidadProductosOfertadosMas5Dias(){
        int cantidad = 0;
        
        try {
            abrirConexion();
            
            Statement st = con.createStatement();
            ResultSet rs =st.executeQuery("SELECT sum(o.stockDisponible) as cantidad\n" +
                                            "from Oferta o \n" +
                                            "where o.diasVigencia > 5");
            if(rs.next()) {
                cantidad = rs.getInt(1);
            }
            rs.close();
            st.close();
            } 
        catch (Exception exc) {
                exc.printStackTrace();
            }
        finally{
                cerrarConexion();
            }
        return cantidad;
    }
    
    public double obtenerMontoPorOferta(int idProducto){
      double monto  = 0;
      
      try {
            abrirConexion();

            String sql = "SELECT sum((o.precioNormal - o.precioOferta) * o.stockDisponible) as monto\n" +
                          "from Oferta o \n" +
                          "where o.idProducto = ?";
            PreparedStatement pt = con.prepareStatement(sql);
            pt.setInt(1, idProducto);
            
            ResultSet rs = pt.executeQuery(); // usa prepareStatement con executeQuery para busqueda con filtro
            
            if(rs.next()){
                monto = rs.getDouble("monto");
            }
            
            rs.close();
            pt.close();
        } 
         catch (Exception exc) {
          exc.printStackTrace();
        }
        finally
        {
            cerrarConexion();
        }
        return monto;
    }
    
}
